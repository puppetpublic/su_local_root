# The su_local_root Puppet module

**Acknowledgement:** The code for this Puppet module was taken in large part from the
`rcadmins` Puppet module written by Karl Kornel.

## Introduction

This module sets up local accounts corresponding to Kerberos root
principals. If the Kerberos root principal is `joestanford/root` then the
local account will have the name `joestanford.root`. These local accounts
have sudo privileges allowing them to act as root on the server. The
authentication required for this sudo is a Duo authentication.

Here is an example of the authentication process after the accounts have
been set up:

1. Get root principal credentials for `joestanford@stanford.edu`:
```
$ kinit -F joestanford/root@stanford.edu
```

2. SSH into the server using the local account corresponding to this root
principal; you will be asked to Duo authenticate before logging in.
```
$ ssh joestanford.root@myserver
Duo two-factor login for joestanford

Enter a passcode or select one of the following options:

 1. Duo Push to XXX-XXX-1234
 2. Duo Push to Duo push (iOS)
 3. Duo Push to XXX-XXX-5678

Passcode or option (1-3): 1
Success. Logging you in...
joestanford.root@integration-test2:~$
```

3. To escalate privilege, use `sudo`:
```
joestanford.root@integration-test2:~$ sudo touch /etc/newfile
Duo two-factor login for joestanford

Enter a passcode or select one of the following options:

 1. Duo Push to XXX-XXX-1234
 2. Duo Push to Duo push (iOS)
 3. Duo Push to XXX-XXX-5678

Passcode or option (1-3): 1

Pushed a login request to your device...
Success. Logging you in...
joestanford.root@integration-test2:~$
```

## Requirements

In order for this to work, you

1. must have a Kerberos root principal, and

1. your regular Duo account (`joestanford@stanford.edu`) must have an alias
corresponding to the local account (`joestanford.root@stanford.edu`).

Note also that this module uses code from the puppetpublic/base> Puppet module.

## How to use the module

1. Include the `su_local_root` class in a Puppet class your server calls:
```
include su_local_root
```

2. In your Hiera configuration file, list all the users who _might_ need
root access to one of your servers. The first parameter is the Linux uid
for that user while the second parameter is the desired shell; a simple
way to get this uid is to look at the entry for the user in the
`/etc/passwd` file.
```
su_local_root::accounts::users:
  joestanford:
    - 12345
    - bash
  janestanford
    - 67890
    - bash
  lelandj:
    - 99999
    - bash
```

3. To instantiate a local root principal account, simply list the above
users above that you want to have root access on that server. For example,
if you want only `janestanford` and `lelandj` to have root access to the server
`janeserver.stanford.edu`, you would add this to the Hiera file for
`janeserver.stanford.edu`:
```
---
# janeserver.stanford.edu.yaml
su_local_root::users:
  - janestanford
  - lelandj
```

## Using a different Kerberos instance when authenticating

The default Kerberos credentials needed to authenticate to the
`joestanford.root` account are the credentials for the `joestanford/root`
Kerberos principal. If you want to use a *different* principal when
authenticating, you can by changing the value of
`su_local_root::instance`. For example, to use `joestanford/superuser`
credentials:
```
su_local_root::instance: superuser
```

**IMPORTANT** If you change the instance from the default of `root` to
something else, the local account name is *still* `joestanford.root` and
the Duo alias needed is *still* `joestanford.root`. The only thing that
is different is that you use the Kerberos credentials for the other
principal.
