# Acknowledgement: The code for this Puppet module was taken in large
# part from the rcadmins Puppet module written by Karl Kornel.

# Create a local root-admin account.
#
# The "name" is the sunetid.
define su_local_root::account (
  Enum['present', 'absent'] $ensure = 'present',
  Integer 		    $uid,
  Integer 		    $gid    = 0,
  String  $shell,
          $supplemental_groups      = [],
  Optional[String] $account_list    = undef,
  Optional[String] $combined_list   = undef,
) {

  if ($ensure == 'absent') {
    user { "${name}.root":
      ensure => 'absent',
    }
  } else {
    # Set groups list by adding "operator" to whatever was provided
    $groups_list = concat($supplemental_groups, ['operator', ])

    # Add a fixed offset to the regular user's UID
    $uid_offset = 1000000

    # Create the .root account, with our special GECOS
    user { "${name}.root":
      ensure     => 'present',
      uid        => $uid + $uid_offset,
      gid        => $gid,
      groups     => $groups_list,
      comment    => $name,
      home       => "/home.root/${name}",
      managehome => true,
      shell      => "/bin/${shell}",
      require    => [ File['/home.root'],
                      Package[$shell],
                    ],
    }

    # If $account_list or $combined_list is set, then add the user to the list.
    if ($account_list) {
      file_line { "${name}.root account list":
        path    => $account_list,
        line    => "${name}.root",
        require => File[$account_list],
      }
    }
    if ($combined_list) {
      file_line {
        "${name} combined list":
          path    => $combined_list,
          line    => $name,
          require => File[$account_list];
        "${name}.root combined list":
          path    => $combined_list,
          line    => "${name}.root",
          require => File[$combined_list];
      }
    }
  }
}
