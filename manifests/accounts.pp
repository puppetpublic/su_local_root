# Acknowledgement: The code for this Puppet module was taken in large
# part from the rcadmins Puppet module written by Karl Kornel.

# Use this class to create cirtual users for all possible local root accounts.
# You can then realize them when you need.
class su_local_root::accounts (
  $users = {},
) {

  # Create a virtual resource for each user.
  $users.each |String $sunetid, Array $values| {
    @su_local_root::account { $sunetid:
      uid     => $values[0],
      shell   => $values[1],
    }
  }

}
