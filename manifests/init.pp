# Acknowledgement: The code for this Puppet module was taken in large
# part from the rcadmins Puppet module written by Karl Kornel.

# $users: a hash mapping sunetids to an array whose first element is the
# user's linux uid and second is the desired shell.

# Example:
#
#  class { 'su_local_root':
#    users => {
#      'joeuser' => [12345, "bash"],
#      'janeroe' => [67890, "tcsh"],
#    }
#  }
#
# $instance: the Kerberos credentials required to login to the local root
#   account. Defaults to 'root' but for some servers other instances might
#   be more appropriate.
# Default: 'root'

class su_local_root (
  Array[String] $users        = [],
  String        $instance     = 'root',
  Boolean       $debuild      = false,
) {

  # From stdlib
  ensure_packages(['bash'], {'ensure' => 'installed'})

  # Bring all the virtual accounts into scope
  include su_local_root::accounts

  # Realize each user and create a home directory.
  # The su_local_root::files define sets up the the .k5login file.
  $users.each |String $sunetid| {
    realize(Su_local_root::Account[$sunetid])
    su_local_root::files { $sunetid:
      instance => $instance,
    }
  }

  # Make sure we have a space for home directories
  file { '/home.root':
    ensure       => 'directory',
    owner        => 'root',
    group        => 'root',
    mode         => '0770',
  }

  # Create a list of "root" users, e.g., if $users is ['joeuser',
  # 'janedoe'] create the array ['joeuser.root', 'janedoe.root'].
  $local_root_users = $users.map |$sunetid| { "${sunetid}.root" }
  class { 'base::sudo':
    duo_sudoers     => $local_root_users,
    duo             => true,
    duo_fail_secure => true,
    duo_gecos       => false,
    debuild         => $debuild,
  }


}
