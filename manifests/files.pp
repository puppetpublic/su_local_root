# Acknowledgement: The code for this Puppet module was taken in large
# part from the rcadmins Puppet module written by Karl Kornel.

# $instance: the Kerberos instance credentials to look for
#   when allowing the .root user to login.
# Default: 'root'

define su_local_root::files (
  String $instance = 'root',
) {
  # Set up a .k5login file
  k5login { "/home.root/${name}/.k5login":
    ensure     => present,
    mode       => '0600',
    purge      => true,
    principals => [ "${name}/${instance}@stanford.edu" ],
    require    => User["${name}.root"]
  }

  # TODO: Put in common files, like .bashrc, .cshrc, etc.
}
